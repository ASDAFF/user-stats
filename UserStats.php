<?php
/**
 * User: Antuan
 */


class UserStats
{
    public static function Init()
    {
        if (!session_id()) {
            session_start();
        }

        if (parse_url($_SERVER["HTTP_REFERER"], PHP_URL_HOST) == $_SERVER['SERVER_NAME']) return;
        if (!empty($_SESSION['stats']['referer']) && parse_url($_SESSION['stats']['referer'], PHP_URL_HOST) == parse_url($_SERVER["HTTP_REFERER"], PHP_URL_HOST)) return;

        $_SESSION['stats'] = array(
            'referer' => $_SERVER["HTTP_REFERER"]
        );
    }

    public static function GetReferer()
    {
        return !empty($_SESSION['stats']['referer']) ? $_SESSION['stats']['referer'] : null;
    }

    public static function GetRefererKeyword()
    {
        if (empty($_SESSION['stats']['referer'])) return null;

        $referer = $_SESSION['stats']['referer'];

        $search_phrase = '';
        $engines = array('dmoz' => 'q=',
            'aol' => 'q=',
            'ask' => 'q=',
            'google' => 'q=',
            'bing' => 'q=',
            'hotbot' => 'q=',
            'teoma' => 'q=',
            'yahoo' => 'p=',
            'altavista' => 'p=',
            'lycos' => 'query=',
            'kanoodle' => 'query=',
            'yandex' => 'text='
        );

        foreach ($engines as $engine => $query_param) {
            // Check if the referer is a search engine from our list.
            // Also check if the query parameter is valid.
            if (strpos($referer, $engine . ".") !== false &&
                strpos($referer, $query_param) !== false) {

                // Grab the keyword from the referer url
                $referer .= "&";
                $pattern = "/[?&]{$query_param}(.*?)&/si";
                preg_match($pattern, $referer, $matches);
                $search_phrase = urldecode($matches[1]);
                return array($engine, $search_phrase);
            }
        }
        return null;
    }

    public static function GetBodyLetter()
    {
        $body_letter = '';
        $lineBreak = '<br>';
        $self_url = (!empty($_SERVER['REQUEST_SCHEME']) ? $_SERVER['REQUEST_SCHEME'] : 'http') . '://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];

        $body_letter .= $lineBreak . $lineBreak . '-- Трекинг пользователя --' . $lineBreak;

        $body_letter .= 'Пользователь пришел на эту страницу с: ' . $_SERVER['HTTP_REFERER'] . $lineBreak;

        if (self::GetReferer() != null) {
            $body_letter .= 'Пользователь пришел на сайт с: ' . self::GetReferer() . $lineBreak;

            if (UserStats::GetRefererKeyword() != null) {
                $body_letter .= "Поисковая фраза: " . UserStats::GetRefererKeyword() . $lineBreak;
            }

        }
		


        $body_letter .= 'Это сообщение отправлено со страницы: ' . $self_url . $lineBreak;

        if (!empty($_SERVER["REMOTE_ADDR"]))
            $body_letter .= 'IP пользователя: ' . $_SERVER["REMOTE_ADDR"] . $lineBreak;

        if (!empty($_SERVER["HTTP_X_FORWARDED_FOR"]) && $_SERVER["HTTP_X_FORWARDED_FOR"] != $_SERVER["REMOTE_ADDR"])
            $body_letter .= 'Proxy Server IP пользователя: ' . $_SERVER["HTTP_X_FORWARDED_FOR"] . $lineBreak;

        if (!empty($_SERVER["HTTP_USER_AGENT"]))
            $body_letter .= 'Браузер пользователя: ' . $_SERVER["HTTP_USER_AGENT"] . $lineBreak;


        return $body_letter;
    }


}